#pragma once
#include <./Macro.h>
#include <./Graphics/GraphicStatus.h>

class InputLayout : public GraphicStatus{
public:
	InputLayout();
	~InputLayout();
	
	bool Create(D3D11_INPUT_ELEMENT_DESC *element,UINT num, ID3D10Blob *blob);
	bool SetStatus();
	bool SetStatus(ID3D11DeviceContext*);

	void AddRef();
	void Release();
private:
	COMPTR(ID3D11InputLayout) inputlayout;
};