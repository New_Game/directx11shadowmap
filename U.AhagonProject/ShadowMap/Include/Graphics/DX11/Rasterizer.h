#pragma once
#include <./Macro.h>
#include <./Graphics/GraphicStatus.h>

class Rasterizer : public GraphicStatus{
public:
	Rasterizer();
	~Rasterizer();

	bool Create(D3D11_RASTERIZER_DESC* desc);
	bool SetStatus();
	bool SetStatus(ID3D11DeviceContext*);

	void AddRef();
	void Release();
	
private:
	COMPTR(ID3D11RasterizerState) rasterizer;

};


