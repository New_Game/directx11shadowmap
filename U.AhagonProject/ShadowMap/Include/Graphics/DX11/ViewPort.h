#pragma once
#include <./Macro.h>
#include <./Graphics/GraphicStatus.h>

class ViewPort : public GraphicStatus
{
public:
	ViewPort();
	~ViewPort();

	bool SetStatus();
	bool SetStatus(ID3D11DeviceContext*);

	void AddRef();
	void Release();
	
	void SetView(float topx, float topy, float Width, float height);
	void SetDepth(float min, float max);

private:
	D3D11_VIEWPORT viewport;

};


