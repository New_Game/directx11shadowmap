#pragma once
#include <Macro.h>
#include <./Resource/Texture.h>
#include <./Graphics/GraphicsPipeLine.h>
#include <./Graphics/DXStates.h>
#include <./Camera/Camera.h>

class ShadowMap {
public:
	static ShadowMap &getInstance() {
		static ShadowMap ins;
		return ins;
	}
	~ShadowMap();
	void Release() {}
	void Active(){}
	int Update();
	bool Create(ULONG width, ULONG height);
	bool CreatePipeLine();
	void Clear();
	void Clear(float color[4]);
	void Clear(float r,float g,float b,float a);
	void Draw();
	void setStatus();
	void setShader();
	Texture2D &getBuffer();

	void setOrtho(bool flg);
	void setProj(bool flg);
	bool isOrtho();
	bool isProj();

	bool isDebug() { return true; }
	float getWidth();
	float getHeight();
private:
	ShadowMap();
	
	GraphicsPipeLine gpl;
	Texture2D buffer;
	TextureDSV2D depth;
	float clearcolor[4];
	COMPTR(ID3D11Buffer) camerabuf;
	COMPTR(ID3D11VertexShader) vs;  // shaderのbuffer　コンパイルしたシェーダーの格納先
	COMPTR(ID3D11PixelShader) ps;  // shaderのbuffer　コンパイルしたシェーダーの格納先
	Game::GCamera camera;
	bool isortho, isproj;

	float fov;
	float NearZ,FarZ;
	
	float camera_pos[3];
	float camera_at[3];
	float width, height;
};