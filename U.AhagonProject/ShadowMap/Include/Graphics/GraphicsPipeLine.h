#pragma once
#include <vector>
#include <./Graphics/GraphicStatus.h>
#include <./Graphics/GraphicObject.h>


class GraphicsPipeLine
{
public:
	GraphicsPipeLine();
	GraphicsPipeLine(unsigned long size);
	~GraphicsPipeLine();

	void setStatus();
	void setStatus(ID3D11DeviceContext*);
	void Release();
	virtual bool Draw();
	virtual bool Attach(GraphicStatus* ptr);
	virtual bool Remove(GraphicStatus* ptr);
	void setPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY);

	// クリアの際に使用する
	static void ClearPipeLine();

protected:
private:
	std::vector<GraphicStatus*> status;
	GraphicObject* object;
	D3D_PRIMITIVE_TOPOLOGY topology;
};
