#pragma once
#include <Macro.h>
#include <d3d11_1.h>
#include <./Resource/Texture.h>
#include <./Resource/Format/EncodingHelper.h>
#include <./Resource/Format/Pmd.h>
#include <./Resource/Format/Pmx.h>
#include <./Resource/Format/Vmd.h>
#include <./Graphics/GraphicsPipeLine.h>

namespace ModelFormat {
namespace PMX {
struct Vertex {
	float positon[3];
	/// 法線
	float normal[3];
	/// テクスチャ座標
	float uv[2];
	/// 追加テクスチャ座標
	float uva[4][4];
};
struct Material {
	/// 減衰色
	float diffuse[4];
	/// 光沢色
	float specular[3];
	/// 光沢度
	float specularlity;
	/// 環境色
	float ambient[3];
	/// エッジ色
	float edge_color[4];
	/// エッジサイズ
	float edge_size;
	float lightvec[4];
	float lightcolor[4];
};
struct Bone {

};
}

namespace PMD {
struct Vertex {
		/// 位置
		float position[3];
		/// 法線
		float normal[3];
		/// UV座標
		float uv[2];
		///// 関連ボーンインデックス
		//uint16_t bone_index[2];
		///// ボーンウェイト
		//uint8_t bone_weight;
		///// エッジ不可視
		//bool edge_invisible;
};
struct Material {
	/// 減衰色
	float diffuse[4];
	/// 光沢度
	float power;
	/// 光沢色
	float specular[3];
	/// 環境色
	float ambient[3];
	///// トーンインデックス
	//uint8_t toon_index;
	///// エッジ
	//uint8_t edge_flag;
	///// インデックス数
	//uint32_t index_count;
	///// テクスチャファイル名
	//std::string texture_filename;
	///// スフィアファイル名
	//std::string sphere_filename;
};
}

}
class Model {
public:
	Model();
	~Model();

	bool Load(std::wstring path);
	bool LoadPmd(std::wstring filepath,std::wstring path);
	bool LoadPmx(std::wstring filepath,std::wstring path);
	bool LoadVmd(std::wstring filepath,std::wstring path);

	bool Release();
	bool Draw();
	bool DrawShadow();
	
	void TextureView();
	void BoneView();
	void _BoneView(int i);
	std::string getName();
	void setDrawShadow(bool);
	bool isDrawShadow();

	float getGlobalScale();
	void setGlobalScale(float sc);
protected:
private:
	float globalscale;
	GraphicsPipeLine gpl;

	enum class FormatType {
		unknown,
		pmx,
		pmd,
		vmd,
		end,
	};
	float lightvec[4];
	float lightcolor[4];
	FormatType formattype;
	std::shared_ptr<pmx::PmxModel>  pmx;
	std::shared_ptr<pmd::PmdModel>  pmd;
	std::shared_ptr<vmd::VmdMotion> vmd;

	COMPTR(ID3D11Buffer) vertex;
	COMPTR(ID3D11Buffer) vertexidx;

	COMPTR(ID3D11VertexShader) vs;
	COMPTR(ID3D11PixelShader) ps;

	COMPTR(ID3D11Buffer) materialbuffer;
	std::vector<Texture2D> texture;//テクスチャ配列
	unsigned int texturenum;//テクスチャ配列
	bool drawshadow;
	//Texture2D shadow;
};