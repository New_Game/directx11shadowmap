#pragma once
#include <map>
#include <functional>

enum Key {
	Enter,
	Cancel,
	Up,
	Down,
	Left,
	Right,
};
struct KeyStatus {
};

class KeyBindMap{
public:
	KeyBindMap();
	~KeyBindMap();
private:
	std::map < Key, std::function<bool(KeyStatus&)>> keymap;
};
