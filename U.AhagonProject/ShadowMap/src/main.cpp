#include <Windows.h>
#include <xnamath.h>
#include <./Device/WindowDevice.h>
#include <./Device/DirectXDevice.h>
#include <./GUI/imgui.h>
#include <./GUI/imgui_internal.h>
#include <./GUI/imgui_impl_dx11.h>

#include <./ReSource/Texture.h>
#include <./ReSource/Model.h>
#include <./Graphics/ShadowMap.h>

// Shaderに送るカメラ情報
struct CameraMtx{
	XMMATRIX mView;			//ビュー変換行列
	XMMATRIX mProjection;	//透視投影変換行列
};
struct WorldMtx{
	XMMATRIX mWorld;		//ワールド変換行列
};
struct LightMtx {
	XMMATRIX mLightView;	//ビュー変換行列
	XMMATRIX mLightProjection;//投資投影変換行列
};


int APIENTRY WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow) {
	WindowDevice &window = WindowDevice::getInstance();
	DX11Device &device = DX11Device::getInstance();
	window.Init(hInstance, nCmdShow, TEXT("U.Ahagon Project"), 1280, 720, true);
	device.Init(window.getHandle(), window.getWidth(), window.getHeight(), window.getWindowMode());
	// IMGUIの初期化
	ImGui_ImplDX11_Init(window.getHandle(), device.getDevice(), device.getContext());

	ShadowMap::getInstance().Create(3000, 3000);
	ShadowMap::getInstance().CreatePipeLine();
	// Shaderに送る行列の生成
	CameraMtx cameramtx;
	WorldMtx worldmtx;
	LightMtx lightmtx;

	//カメラ
	XMVECTOR hEye = XMVectorSet(0.0f, 4.0f, -5.0f, 0.0f);
	XMVECTOR hAt = XMVectorSet(0.0f, 1.5f, 0.0f, 0.0f);
	XMVECTOR hUp = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	cameramtx.mView = XMMatrixLookAtLH(hEye, hAt, hUp);
	cameramtx.mProjection = XMMatrixPerspectiveFovLH(D3DXToRadian(45.0f), (float) window.getWidth()/(float) window.getHeight(), 0.1f, 1000);
	cameramtx.mView = XMMatrixTranspose(cameramtx.mView);
	cameramtx.mProjection = XMMatrixTranspose(cameramtx.mProjection);

	//ワールド
	worldmtx.mWorld = XMMatrixIdentity();
	worldmtx.mWorld = XMMatrixScaling(0.1f, 0.1f, 0.1f);
	worldmtx.mWorld = XMMatrixTranspose(worldmtx.mWorld);

	//ライト
	XMVECTOR hlEye = XMVectorSet(10.0f, 30.0f, 0.0f, 0.0f);
	XMVECTOR hlAt = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
	XMVECTOR hlUp = XMVectorSet(0.0f, 1.0f, 0.0f,0.0f);
	
	float lightfov = 45.0f;
	float lightpos[4] = { 2.0f,3.0f,-1.0f,0.0f };
	float lightAt[4] = { 0.0f,0.0f,0.0f,0.0f };
	
	//コンテキストバッファ：シェーダーで宣言した定数をプログラム側から変更する(主に生成、更新、シェーダーステージへのセットという３つのアクション)
	// constantバッファ生成
	D3D11_BUFFER_DESC bd; // 生成方法(バッファー リソース)
	ID3D11Buffer *cameraconstantbuffer = nullptr;
	ID3D11Buffer *worldconstantbuffer = nullptr;
	ID3D11Buffer *lightconstantbuffer = nullptr;
	ZeroMemory(&bd, sizeof(bd)); // 中身をクリア
	// Bufferの生成方法の格納
	bd.ByteWidth = sizeof(CameraMtx); // sizeの指定
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER; // なんのバッファですか？
	bd.CPUAccessFlags = 0; // CPUからは書き込みのみ行います
	bd.MiscFlags = 0;
	bd.StructureByteStride = sizeof(float);
	device.getDevice()->CreateBuffer(&bd, NULL, &cameraconstantbuffer); // バッファの生成

	ZeroMemory(&bd, sizeof(bd)); // 中身をクリア
	// Bufferの生成方法の格納
	bd.ByteWidth = sizeof(WorldMtx); // sizeの指定
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER; // なんのバッファですか？
	bd.CPUAccessFlags = 0; // CPUからは書き込みのみ行います
	bd.MiscFlags = 0;
	bd.StructureByteStride = sizeof(float);

	device.getDevice()->CreateBuffer(&bd, NULL, &worldconstantbuffer); // バッファの生成

	ZeroMemory(&bd, sizeof(bd)); // 中身をクリア
	// Bufferの生成方法の格納
	bd.ByteWidth = sizeof(LightMtx); // sizeの指定
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER; // なんのバッファですか？
	bd.CPUAccessFlags = 0; // CPUからは書き込みのみ行います
	bd.MiscFlags = 0;
	bd.StructureByteStride = sizeof(float);

	device.getDevice()->CreateBuffer(&bd, NULL, &lightconstantbuffer); // バッファの生成



	Texture2D tex,tex2,tex3;
	tex.LoadFile("./ReSource/ahagon.png");
	tex2.LoadFile("./ReSource/ahagon2.png");
	tex3.LoadFile("./ReSource/neneth.jpg");
	
	float cnt = 0;
	float cnt_max = 60*30;
	float cnt_add = 1;
	auto Bezier = [](float t,float a,float b,float c) {
		float f = 1 - t;
		f = t*t*c + 2 * t*f*b + f*f*a;
		return f;
	};


	Model model;
	Model floor;
	model.Load(L"./Resource/Model/miku/miku.pmx");
	//floor.Load(L"./Resource/Model/room/room.pmx");
	floor.Load(L"./Resource/Model/輝夜姫ステージ/輝夜姫ステージ.pmx");
	int ret = 0;

	ShadowMap& shadow = ShadowMap::getInstance();
	do{
		float clear [] = { 0.0f, 0.0f, 0.0f, 0.0f };
		device.getContext()->ClearRenderTargetView(device.getRTV(), clear);
		device.getContext()->ClearDepthStencilView(device.getDSV(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
		shadow.Clear();
		ImGui_ImplDX11_NewFrame();
		
		//ImGui::ShowTestWindow();

		ImGui::Text("Application.average %.3f ms/frame(%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate); // FPS

		////下らぬ物達
		//if(false){
		//	float s = Bezier( cnt / cnt_max, 0.0f, 0.25f, 0.0f);
		//	static bool show = false;
		//	if (show && cnt > 60 * 10 && cnt < 60 * 20) {
		//		ImGui::SetNextWindowPos(ImVec2());
		//		ImGui::SetNextWindowSize(ImVec2(window.getWidth(), window.getHeight()));
		//		ImGui::Begin("neneth", nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize| ImGuiWindowFlags_NoTitleBar);
		//		ImGui::Image((void*) tex3.getSRV(), ImVec2(window.getWidth(), window.getHeight()));
		//		ImGui::End();
		//	}
		//	ImGui::SetNextWindowPos(ImVec2(300 * s, 300 * s));
		//	if (show) {
		//		ImGui::SetNextWindowSize(ImVec2(window.getWidth(), window.getHeight()));
		//	}
		//	else {
		//		ImGui::SetNextWindowSize(ImVec2(599*0.55f, 337*0.55f));
		//	}
		//	ImGui::Begin("Ahagon", nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize| ImGuiWindowFlags_NoTitleBar);
		//	ImGui::SetCursorPos(ImVec2());
		//	if (ImGui::ImageButton((void*) tex.getSRV(), ImVec2(599 * (0.5f - s), 337 * (0.5f - s)))) {
		//		show = !show;
		//	}
		//	if (show) {
		//		ImGui::SetCursorScreenPos(ImVec2(window.getWidth()/2 - 250 * (1.0f - s), window.getHeight()/2 - 200 * (1.0f - s)));
		//		ImGui::Image((void*) tex2.getSRV(), ImVec2(500 * (1.0f - s), 400 * (1.0f - s)));
		//	}
		//	if (cnt < 0  || cnt_max < cnt) {
		//		cnt_add *= -1;
		//	}
		//	cnt += cnt_add;
		//	ImGui::End();
		//}

		ShadowMap::getInstance().Update();
		//モデル描画
		{
			ImGui::Begin("Light", nullptr, ImGuiWindowFlags_NoScrollbar);
			ImGui::DragFloat3("Light Pos", lightpos, 0.05f);
			ImGui::DragFloat3("Light LookAt", lightAt, 0.05f);
			ImGui::DragFloat("Light Fov", &lightfov, 0.01f);
			ImGui::End();

			hlEye = XMVectorSet(lightpos[0], lightpos[1], lightpos[2], lightpos[3]);
			hlAt  = XMVectorSet(lightAt[0], lightAt[1], lightAt[2], lightAt[3]);

			lightmtx.mLightView = XMMatrixLookAtLH(hlEye, hlAt, hlUp);
			lightmtx.mLightProjection = XMMatrixPerspectiveFovLH(D3DXToRadian(lightfov), shadow.getWidth() / shadow.getHeight(), 0.1f, 1000.0f);
			lightmtx.mLightView = XMMatrixTranspose(lightmtx.mLightView);
			lightmtx.mLightProjection = XMMatrixTranspose(lightmtx.mLightProjection);

			device.getContext()->UpdateSubresource(cameraconstantbuffer, 0, NULL, &cameramtx, 0, 0); // CPU によって、マッピング不可能なメモリー内に作成されたサブリソースにメモリーからデータがコピーされる
			device.getContext()->UpdateSubresource(worldconstantbuffer, 0, NULL, &worldmtx, 0, 0); // CPU によって、マッピング不可能なメモリー内に作成されたサブリソースにメモリーからデータがコピーされる
			device.getContext()->UpdateSubresource(lightconstantbuffer, 0, NULL, &lightmtx, 0, 0); // CPU によって、マッピング不可能なメモリー内に作成されたサブリソースにメモリーからデータがコピーされる
			device.getContext()->VSSetConstantBuffers(0, 1, &cameraconstantbuffer); // 頂点シェーダーのパイプライン ステージで使用される定数バッファーを設定
			device.getContext()->VSSetConstantBuffers(1, 1, &worldconstantbuffer); // 頂点シェーダーのパイプライン ステージで使用される定数バッファーを設定
			device.getContext()->VSSetConstantBuffers(2, 1, &lightconstantbuffer); // 頂点シェーダーのパイプライン ステージで使用される定数バッファーを設定
			//floor.DrawShadow();
			model.DrawShadow();
			model.Draw();
			floor.Draw();
			

			/*ImGui::Begin("Shadow");
			void *p = ShadowMap::getInstance().getBuffer().getSRV();
			ImGui::Image(p,ImVec2(500,500));
			ImGui::End();*/
		}
		
		

		//Guiの描画
		{
			DEBUG(device.getAnotation()->BeginEvent(L"ImGui"));
			ID3D11RenderTargetView *rtv[] = {
				device.getRTV(),
			};
			device.getContext()->OMSetRenderTargets(1, rtv, nullptr);
			ImGui::Render();
			DEBUG(device.getAnotation()->EndEvent());
		}
		device.getSwapChain()->Present(0, 0);
		ret = window.MessageLoop();
		

	}while (ret != WM_QUIT);

	ImGui_ImplDX11_Shutdown();
	model.Release();
	floor.Release();
	// マクロリリース
	SAFE_RELEASE(cameraconstantbuffer);
	SAFE_RELEASE(worldconstantbuffer);
	SAFE_RELEASE(lightconstantbuffer);
}

