#include <Utility.h>
#include <./Resource/Model.h>
#include <algorithm>
#include <./Device/DirectXDevice.h>
#include <./Graphics/DXStates.h>
#include <xnamath.h>
#include <./Device/WindowDevice.h>
#include <./Graphics/ShadowMap.h>
#include <./GUI/imgui.h>
#include <./GUI/imgui_internal.h>
#include <./GUI/imgui_impl_dx11.h>

#pragma warning(disable:4996)
struct ConstantBuffer {
	XMMATRIX mWorld;		//ワールド変換行列
	XMMATRIX mView;			//ビュー変換行列
	XMMATRIX mProjection;	//透視投影変換行列
};

Model::Model() {
	this->formattype = FormatType::unknown;
	this->drawshadow = true;
	this->setGlobalScale(1.0f);
	this->lightvec[0] = 1.0f;
	this->lightvec[1] = 1.0f;
	this->lightvec[2] = 1.0f;
	this->lightvec[3] = 1.0f;

	this->lightcolor[0] = 1.0f;
	this->lightcolor[1] = 1.0f;
	this->lightcolor[2] = 1.0f;
	this->lightcolor[3] = 1.0f;
}
Model::~Model() {
	this->formattype = FormatType::unknown;

}
bool Model::Release() {
	if (this->formattype == FormatType::unknown)
		return false;
	switch (this->formattype) {
	case FormatType::pmd:
	this->pmd.reset();
	break;
	case FormatType::pmx:
	this->pmx.reset();
	break;
	case FormatType::vmd:
	this->vmd.reset();
	break;
	}
	this->gpl.Release();
	return true;
}
bool Model::Load(std::wstring path) {
	if (this->formattype != FormatType::unknown) {
		return false;
	}
	int ext_i = path.find_last_of(L".");//10
	int path_i = path.find_last_of(L"\\") + 1;//7
	if (path_i - 1 == std::wstring::npos) {
		path_i = path.find_last_of(L"/") + 1;//7
	}
	std::wstring extname = path.substr(ext_i, path.size() - ext_i);
	std::wstring filepath = path.substr(0, path_i);
	std::transform(extname.cbegin(), extname.cend(), extname.begin(), tolower);
	if (extname == L".pmd") {
		return this->LoadPmd(path, filepath);
	}
	else if (extname == L".pmx") {
		return this->LoadPmx(path, filepath);
	}
	else if (extname == L".vmd") {
		return this->LoadVmd(path, filepath);
	}
	return false;
}
bool Model::LoadPmd(std::wstring filepath, std::wstring path) {
	DX11Device &device = DX11Device::getInstance();
	WindowDevice window = WindowDevice::getInstance();
	this->formattype = FormatType::pmd;
	if (!this->pmd) {
		std::shared_ptr<pmd::PmdModel> pmd(new pmd::PmdModel());
		this->pmd = std::move(pmd);
	}
	std::ifstream stream = std::ifstream(filepath.c_str(), std::ios_base::binary);
	this->pmd->LoadFromStream(&stream);
	stream.close();

	ID3D11Buffer *vertexbuffer = nullptr;
	ID3D11Buffer *vertexidxbuffer = nullptr;
	ID3D11Buffer *constantbuffer = nullptr;
	D3D11_BUFFER_DESC bd; // 生成方法(バッファー リソース)
	ZeroMemory(&bd, sizeof(bd)); // 中身をゼロクリア
	// Bufferの生成方法の格納
	bd.Usage = D3D11_USAGE_DYNAMIC; // バッファーで想定されている読み込みおよび書き込みの方法を識別
	bd.ByteWidth = sizeof(ModelFormat::PMD::Vertex)*this->pmd->vertices.size();  // バッファーのサイズ(バイト単位)
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER; // なんのバッファですか？
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE; // CPUからは書き込みのみ行います
	// Bufferの生成
	device.getDevice()->CreateBuffer(&bd, nullptr, &vertexbuffer);
	bd.ByteWidth = sizeof(uint16_t)*this->pmd->indices.size();  // バッファーのサイズ(バイト単位)
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER; // なんのバッファですか？
	device.getDevice()->CreateBuffer(&bd, nullptr, &vertexidxbuffer);

	bd.ByteWidth = sizeof(ModelFormat::PMD::Material);  // バッファーのサイズ(バイト単位)
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER; // なんのバッファですか？
	device.getDevice()->CreateBuffer(&bd, nullptr, &constantbuffer);


	// 頂点情報を格納していく
	D3D11_MAPPED_SUBRESOURCE ms; // Bufferを格納する為にとりあえずロックをかけないといけない。どこまでロックをかける？サブリソース データにアクセスできるようにする
	device.getContext()->Map(vertexbuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms); // アクセス先ms
	ModelFormat::PMD::Vertex *pvertex = (ModelFormat::PMD::Vertex*)ms.pData;
	for (unsigned int i = 0; i < this->pmd->vertices.size(); i++) {
		memcpy(&pvertex[i], &this->pmd->vertices[i], 12 + 12 + 8 + 16 + 16 + 16 + 16 + 16);
	}
	device.getContext()->Unmap(vertexbuffer, NULL); // ロック解除

	// index情報を格納していく
	device.getContext()->Map(vertexidxbuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms); // アクセス先ms
	memcpy(ms.pData, &this->pmd->indices[0], sizeof(uint16_t) *this->pmd->indices.size());
	device.getContext()->Unmap(vertexidxbuffer, NULL); // ロック解除

	this->vertex = std::move(vertexbuffer);
	this->vertexidx = std::move(vertexidxbuffer);
	this->materialbuffer = std::move(constantbuffer);

	//仮

	ID3D11VertexShader *vs_buf;
	ID3D11PixelShader  *ps_buf;
	ID3D10Blob *vsblob, *psblob; // 任意長のデータを返す際に使用
	// ファイルを元にエフェクトをコンパイル
	D3DX11CompileFromFile(TEXT("./Shader/model/PMD/VS.hlsl"), 0, 0, "main", "vs_5_0", 0, 0, 0, &vsblob, 0, 0);
	D3DX11CompileFromFile(TEXT("./Shader/model/PMD/PS.hlsl"), 0, 0, "main", "ps_5_0", 0, 0, 0, &psblob, 0, 0);
	// blobを_bufに格納
	device.getDevice()->CreateVertexShader(vsblob->GetBufferPointer(), vsblob->GetBufferSize(), nullptr, &vs_buf); // コンパイル済みシェーダーから、頂点シェーダー オブジェクトを作成
	device.getDevice()->CreatePixelShader(psblob->GetBufferPointer(), psblob->GetBufferSize(), nullptr, &ps_buf);  // ピクセル シェーダーを作成


	D3D11_INPUT_ELEMENT_DESC element [] = { // 入力アセンブラー ステージの単一の要素( HLSL セマンティクス,要素のセマンティクス インデックス,要素データのデータ型,入力アセンブラーを識別する整数値,各要素間のオフセット (バイト単位),単一の入力スロットの入力データ クラスを識別,インスタンス単位の同じデータを使用して描画するインスタンスの数)
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },// POS情報
		{ "NORMAL"	, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,	12, D3D11_INPUT_PER_VERTEX_DATA, 0 },// NORMAL情報
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0,		12 + 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }, // UV情報
	};
	InputLayout *inputlayout = new InputLayout;
	inputlayout->Create(element, ARRAYSIZE(element), vsblob);
	vsblob->Release();
	psblob->Release();

	this->ps = std::move(ps_buf);
	this->vs = std::move(vs_buf);

	this->gpl.Attach(inputlayout);

	D3D11_RASTERIZER_DESC rasterizerDesc = {
		D3D11_FILL_SOLID, // ワイヤーフレーム (レンダリング時に使用する描画モードを決定)
		D3D11_CULL_BACK, // 裏面ポリゴンをカリング(指定の方向を向いている三角形が描画されないことを示す)
		FALSE,			  // 三角形が前向きか後ろ向きかを決定する
		0,				  // 指定のピクセルに加算する深度値
		0.0f,             // ピクセルの最大深度バイアス
		FALSE,			  // 指定のピクセルのスロープに対するスカラ
		FALSE,			  // 距離に基づいてクリッピングを有効
		FALSE,            // シザー矩形カリングを有効
		FALSE,			  // マルチサンプリングのアンチエイリアシングを有効
		TRUE			  //　線のアンチエイリアシングを有効
	};
	Rasterizer *rast = new Rasterizer;
	// ラスタライザの生成
	rast->Create(&rasterizerDesc);
	// パイプラインにラスタライザを追加
	this->gpl.Attach(rast);
	this->gpl.setPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// サンプラーの生成
	D3D11_SAMPLER_DESC sampDesc;
	//サンプラーの設定
	sampDesc.Filter = D3D11_FILTER_ANISOTROPIC;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.BorderColor[0] = 0.0f;
	sampDesc.BorderColor[1] = 0.0f;
	sampDesc.BorderColor[2] = 0.0f;
	sampDesc.BorderColor[3] = 0.0f;
	sampDesc.MipLODBias = 0.0f;
	sampDesc.MaxAnisotropy = 2;
	sampDesc.MinLOD = FLT_MAX * -1;
	sampDesc.MaxLOD = FLT_MAX;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	Sampler *samp = new Sampler;
	samp->Create(0, &sampDesc);
	samp->Attach(1, samp, 0);
	samp->Attach(2, samp, 0);
	samp->Attach(3, samp, 0);
	samp->SetStatus();
	this->gpl.Attach(samp);

	for (unsigned int i = 0; i < this->pmd->materials.size(); i++) {
		Texture2D tex;
		std::wstring file;
		widen(this->pmd->materials[i].texture_filename, file);
		tex.LoadFile(path + file);
		this->texture.push_back(tex);
	}
	this->texturenum = this->texture.size();

	ViewPort *viewport = new ViewPort();
	viewport->SetView(0, 0, (float) window.getWidth(), (float) window.getHeight());
	viewport->SetDepth(0, 1.0f);
	this->gpl.Attach(viewport);

	return true;
}
bool Model::LoadPmx(std::wstring filepath, std::wstring path) {
	DX11Device &device = DX11Device::getInstance();
	WindowDevice window = WindowDevice::getInstance();
	this->formattype = FormatType::pmx;
	//pmxのモデルは大きいのでグローバルスケールを小さくしておく
	this->setGlobalScale(0.1f);

	if (!this->pmx) {
		std::shared_ptr<pmx::PmxModel> pmx(new pmx::PmxModel());
		this->pmx = std::move(pmx);
	}

	std::ifstream stream = std::ifstream(filepath.c_str(), std::ios_base::binary);
	this->pmx->Read(&stream);
	stream.close();

	//this->pmx->vertex_count
	ID3D11Buffer *vertexbuffer = nullptr;
	ID3D11Buffer *vertexidxbuffer = nullptr;
	ID3D11Buffer *constantbuffer = nullptr;
	D3D11_BUFFER_DESC bd; // 生成方法(バッファー リソース)
	ZeroMemory(&bd, sizeof(bd)); // 中身をゼロクリア
	// Bufferの生成方法の格納
	bd.Usage = D3D11_USAGE_DYNAMIC; // バッファーで想定されている読み込みおよび書き込みの方法を識別
	bd.ByteWidth = sizeof(ModelFormat::PMX::Vertex)*this->pmx->vertex_count;  // バッファーのサイズ(バイト単位)
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER; // なんのバッファですか？
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE; // CPUからは書き込みのみ行います
	// Bufferの生成
	device.getDevice()->CreateBuffer(&bd, nullptr, &vertexbuffer);
	bd.ByteWidth = sizeof(int)*this->pmx->index_count;  // バッファーのサイズ(バイト単位)
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER; // なんのバッファですか？
	device.getDevice()->CreateBuffer(&bd, nullptr, &vertexidxbuffer);

	bd.ByteWidth = sizeof(ModelFormat::PMX::Material);  // バッファーのサイズ(バイト単位)
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER; // なんのバッファですか？
	device.getDevice()->CreateBuffer(&bd, nullptr, &constantbuffer);


	// 頂点情報を格納していく
	D3D11_MAPPED_SUBRESOURCE ms; // Bufferを格納する為にとりあえずロックをかけないといけない。どこまでロックをかける？サブリソース データにアクセスできるようにする
	device.getContext()->Map(vertexbuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms); // アクセス先ms
	ModelFormat::PMX::Vertex *pvertex = (ModelFormat::PMX::Vertex*)ms.pData;
	for (int i = 0; i < this->pmx->vertex_count; i++) {
		memcpy(&pvertex[i], &this->pmx->vertices[i], 12 + 12 + 8 + 16 + 16 + 16 + 16 + 16);
	}
	device.getContext()->Unmap(vertexbuffer, NULL); // ロック解除

	// index情報を格納していく
	device.getContext()->Map(vertexidxbuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms); // アクセス先ms
	memcpy(ms.pData, this->pmx->indices.get(), sizeof(int) *this->pmx->index_count);
	device.getContext()->Unmap(vertexidxbuffer, NULL); // ロック解除

	this->vertex = std::move(vertexbuffer);
	this->vertexidx = std::move(vertexidxbuffer);
	this->materialbuffer = std::move(constantbuffer);

	//仮

	ID3D11VertexShader *vs_buf;
	ID3D11PixelShader  *ps_buf;
	ID3D10Blob *vsblob, *psblob; // 任意長のデータを返す際に使用
	// ファイルを元にエフェクトをコンパイル
	D3DX11CompileFromFile(TEXT("./Shader/model/PMX/VS.hlsl"), 0, 0, "main", "vs_5_0", 0, 0, 0, &vsblob, 0, 0);
	D3DX11CompileFromFile(TEXT("./Shader/model/PMX/PS.hlsl"), 0, 0, "main", "ps_5_0", 0, 0, 0, &psblob, 0, 0);
	// blobを_bufに格納
	device.getDevice()->CreateVertexShader(vsblob->GetBufferPointer(), vsblob->GetBufferSize(), nullptr, &vs_buf); // コンパイル済みシェーダーから、頂点シェーダー オブジェクトを作成
	device.getDevice()->CreatePixelShader(psblob->GetBufferPointer(), psblob->GetBufferSize(), nullptr, &ps_buf);  // ピクセル シェーダーを作成


	D3D11_INPUT_ELEMENT_DESC element [] = { // 入力アセンブラー ステージの単一の要素( HLSL セマンティクス,要素のセマンティクス インデックス,要素データのデータ型,入力アセンブラーを識別する整数値,各要素間のオフセット (バイト単位),単一の入力スロットの入力データ クラスを識別,インスタンス単位の同じデータを使用して描画するインスタンスの数)
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },// POS情報
		{ "NORMAL"	, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,	12, D3D11_INPUT_PER_VERTEX_DATA, 0 },// NORMAL情報
		{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0,		12 + 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }, // UV情報 
		{ "UVA"	, 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,	12 + 12 + 8 + 16, D3D11_INPUT_PER_VERTEX_DATA, 0 }, // UV情報 
		{ "UVA"	, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,	12 + 12 + 8 + 16 + 16, D3D11_INPUT_PER_VERTEX_DATA, 0 }, // UV情報 
		{ "UVA"	, 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,	12 + 12 + 8 + 16 + 16 + 16, D3D11_INPUT_PER_VERTEX_DATA, 0 }, // UV情報 
		{ "UVA"	, 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,	12 + 12 + 8 + 16 + 16 + 16 + 16, D3D11_INPUT_PER_VERTEX_DATA, 0 }, // UV情報 
	};
	InputLayout *inputlayout = new InputLayout;
	inputlayout->Create(element, ARRAYSIZE(element), vsblob);
	vsblob->Release();
	psblob->Release();

	this->ps = std::move(ps_buf);
	this->vs = std::move(vs_buf);

	this->gpl.Attach(inputlayout);

	D3D11_RASTERIZER_DESC rasterizerDesc = {
		D3D11_FILL_SOLID, // ワイヤーフレーム (レンダリング時に使用する描画モードを決定)
		D3D11_CULL_BACK, // 裏面ポリゴンをカリング(指定の方向を向いている三角形が描画されないことを示す)
		FALSE,			  // 三角形が前向きか後ろ向きかを決定する
		0,				  // 指定のピクセルに加算する深度値
		0.0f,             // ピクセルの最大深度バイアス
		FALSE,			  // 指定のピクセルのスロープに対するスカラ
		FALSE,			  // 距離に基づいてクリッピングを有効
		FALSE,            // シザー矩形カリングを有効
		FALSE,			  // マルチサンプリングのアンチエイリアシングを有効
		TRUE			  //　線のアンチエイリアシングを有効
	};
	Rasterizer *rast = new Rasterizer;
	// ラスタライザの生成
	rast->Create(&rasterizerDesc);
	// パイプラインにラスタライザを追加
	this->gpl.Attach(rast);
	this->gpl.setPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// サンプラーの生成
	D3D11_SAMPLER_DESC sampDesc;
	//サンプラーの設定
	sampDesc.Filter = D3D11_FILTER_ANISOTROPIC;
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.BorderColor[0] = 0.0f;
	sampDesc.BorderColor[1] = 0.0f;
	sampDesc.BorderColor[2] = 0.0f;
	sampDesc.BorderColor[3] = 0.0f;
	sampDesc.MipLODBias = 0.0f;
	sampDesc.MaxAnisotropy = 2;
	sampDesc.MinLOD = FLT_MAX * -1;
	sampDesc.MaxLOD = FLT_MAX;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
	Sampler *samp = new Sampler;
	samp->Create(0, &sampDesc);
	samp->Attach(1, samp, 0);
	samp->Attach(2, samp, 0);
	samp->Attach(3, samp, 0);
	samp->SetStatus();
	this->gpl.Attach(samp);

	for (int i = 0; i < this->pmx->texture_count; i++) {
		Texture2D tex;
		tex.LoadFile(path + this->pmx->textures[i]);
		this->texture.push_back(tex);
	}
	this->texturenum = this->texture.size();

	ViewPort *viewport = new ViewPort();
	viewport->SetView(0, 0, (float) window.getWidth(), (float) window.getHeight());
	viewport->SetDepth(0, 1.0f);
	this->gpl.Attach(viewport);
	return true;
}
bool Model::LoadVmd(std::wstring filepath, std::wstring path) {
	this->formattype = FormatType::vmd;

	if (!this->vmd) {
		std::shared_ptr<vmd::VmdMotion> vmd(new vmd::VmdMotion());
		this->vmd = std::move(vmd);
	}

	std::ifstream stream = std::ifstream(filepath.c_str(), std::ios_base::binary);
	this->vmd->LoadFromStream(&stream);
	stream.close();

	return true;
}
std::string Model::getName() {
	std::string name;
	narrow(this->pmx->model_name, name);
	return name;
}
bool Model::Draw() {
	if (this->formattype == Model::FormatType::unknown)
		return false;
	DX11Device &device = DX11Device::getInstance();
	//Deferred &deferred = Deferred::getInstance();

	//if (this->drawshadow) {
	//	this->DrawShadow();
	//}
	ImGui::Begin(("test."+ std::to_string((int)this)).c_str(), nullptr, ImGuiWindowFlags_NoScrollbar);
	ImGui::DragFloat4("LightVec", this->lightvec, 0.01f);
	ImGui::DragFloat4("LightColor", this->lightcolor, 0.01f);
	ImGui::End();

	if (this->formattype == Model::FormatType::pmx) {

		DEBUG(device.getAnotation()->BeginEvent(this->pmx->model_name.c_str()));
		this->gpl.ClearPipeLine();
		this->gpl.setStatus();

		UINT stride = sizeof(ModelFormat::PMX::Vertex); // 頂点のサイズ
		UINT offset = 0;			   // ずれの調整
		ID3D11Buffer *vexbuf = this->vertex.GetInterfacePtr();
		device.getContext()->IASetVertexBuffers(0, 1, &vexbuf, &stride, &offset);
		device.getContext()->IASetIndexBuffer(this->vertexidx.GetInterfacePtr(), DXGI_FORMAT_R32_UINT, 0);
		//device.getContext()->OMSetRenderTargets(4, deferred.GBufferRTV(), device.getDSV());
		ID3D11RenderTargetView *rtv [] = {
			device.getRTV()
		};
		device.getContext()->OMSetRenderTargets(1, rtv, device.getDSV());


		

		//仮組み
		device.getContext()->VSSetShader(this->vs.GetInterfacePtr(), nullptr, 0); // 頂点シェーダーをデバイスに設定
		device.getContext()->PSSetShader(this->ps.GetInterfacePtr(), nullptr, 0); // ピクセル シェーダーをデバイスに設定

		

		unsigned long count = 0;
		for (int i = 0; i < this->pmx->material_count; i++) {
			if (!this->pmx->materials[i].flag) {
				count += this->pmx->materials[i].index_count;
				continue;
			}
			DEBUG(device.getAnotation()->BeginEvent(this->pmx->materials[i].material_name.c_str()));

			ID3D11ShaderResourceView *srv[4];
			if ((unsigned)this->pmx->materials[i].diffuse_texture_index < this->texture.size()) {
				srv[0] = this->texture[this->pmx->materials[i].diffuse_texture_index].getSRV();
			}
			else {
				srv[0] = nullptr;
			}
			if ((unsigned)this->pmx->materials[i].toon_texture_index < this->texture.size()) {
				srv[1] = this->texture[this->pmx->materials[i].toon_texture_index].getSRV();
			}
			else {
				srv[1] = nullptr;
			}
			if ((unsigned)this->pmx->materials[i].toon_texture_index < this->texture.size()) {
				srv[2] = this->texture[this->pmx->materials[i].toon_texture_index].getSRV();
			}
			else {
				srv[2] = nullptr;
			}
			if (ShadowMap::getInstance().getBuffer().getSRV()) {
				srv[3] = ShadowMap::getInstance().getBuffer().getSRV();
			}
			else {
				srv[3] = nullptr;
			}
			device.getContext()->PSSetShaderResources(0, sizeof(srv) / sizeof(ID3D11ShaderResourceView*), srv);

			// 頂点情報を格納していく
			D3D11_MAPPED_SUBRESOURCE ms; // Bufferを格納する為にとりあえずロックをかけないといけない。どこまでロックをかける？サブリソース データにアクセスできるようにする
			device.getContext()->Map(this->materialbuffer.GetInterfacePtr(), NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms); // アクセス先ms
			ModelFormat::PMX::Material *pmaterial = (ModelFormat::PMX::Material*)ms.pData;
			memcpy(pmaterial->diffuse, this->pmx->materials[i].diffuse, sizeof(float) * 4);
			memcpy(pmaterial->specular, this->pmx->materials[i].specular, sizeof(float) * 3);
			memcpy(&pmaterial->specularlity, &this->pmx->materials[i].specularlity, sizeof(float));
			memcpy(pmaterial->ambient, this->pmx->materials[i].ambient, sizeof(float) * 3);
			memcpy(pmaterial->edge_color, this->pmx->materials[i].edge_color, sizeof(float) * 4);
			memcpy(&pmaterial->edge_size, &this->pmx->materials[i].edge_size, sizeof(float));
			memcpy(pmaterial->lightvec, this->lightvec, sizeof(float) * 4);
			memcpy(pmaterial->lightcolor, this->lightcolor, sizeof(float) * 4);
			device.getContext()->Unmap(this->materialbuffer.GetInterfacePtr(), NULL); // ロック解除
			ID3D11Buffer *buf [] = {
				 this->materialbuffer.GetInterfacePtr(),
			};
			device.getContext()->PSSetConstantBuffers(0, 1, buf);

			device.getContext()->DrawIndexed(this->pmx->materials[i].index_count, count, 0);
			count += this->pmx->materials[i].index_count;
			DEBUG(device.getAnotation()->EndEvent());
		}

		DEBUG(device.getAnotation()->EndEvent());

	}
	
	return true;
}
bool Model::DrawShadow() {
	
	if (this->formattype == Model::FormatType::unknown)
		return false;
	DX11Device &device = DX11Device::getInstance();
	ShadowMap &shadowmap = ShadowMap::getInstance();

	if (this->formattype == Model::FormatType::pmx) {
		
		DEBUG(device.getAnotation()->BeginEvent((L"Shadow_"+this->pmx->model_name).c_str()));
		//仮組み
		shadowmap.setStatus();
		shadowmap.setShader();
		//バッファはセット済みのためそのまま流用
		UINT stride = sizeof(ModelFormat::PMX::Vertex); // 頂点のサイズ
		UINT offset = 0;			   // ずれの調整
		ID3D11Buffer *vexbuf = this->vertex.GetInterfacePtr();
		device.getContext()->IASetVertexBuffers(0, 1, &vexbuf, &stride, &offset);
		device.getContext()->IASetIndexBuffer(this->vertexidx.GetInterfacePtr(), DXGI_FORMAT_R32_UINT, 0);
		unsigned long count = 0;
		for (int i = 0; i < this->pmx->material_count; i++) {
			if (!this->pmx->materials[i].flag) {
				count += this->pmx->materials[i].index_count;
				continue;
			}
			DEBUG(device.getAnotation()->BeginEvent((L"Shadow_"+this->pmx->materials[i].material_name).c_str()));
			device.getContext()->DrawIndexed(this->pmx->materials[i].index_count, count, 0);
			count += this->pmx->materials[i].index_count;
			DEBUG(device.getAnotation()->EndEvent());
		}

		DEBUG(device.getAnotation()->EndEvent());

	}
	else if (this->formattype == Model::FormatType::pmd) {

	}
	return true;
}

void Model::TextureView() {
	if (this->formattype == Model::FormatType::unknown)
		return;

	ImGui::PushID((int)this->pmx.get());
	if (ImGui::CollapsingHeader("Model"))
	{
		if (ImGui::CollapsingHeader(("Texture")))
		{
			for (unsigned int i = 0; i < this->texture.size(); i++) {
				ImGui::Image(this->texture[i].getSRV(), ImVec2(256, 256));
			}
		}
	}
	ImGui::PopID();
}
void Model::BoneView() {
	if (this->pmx->bone_count <= 0)
		return;
	if (ImGui::TreeNode("Bone"))
	{
		for (unsigned int i = 0; i < (unsigned)this->pmx->bone_count; i++) {
			if (this->pmx->bones[i].parent_index != -1)
				continue;
			std::string name;
			narrow(this->pmx->bones[i].bone_english_name, name);
			if (name == "") {
				name = "UnkownBone : " + std::to_string(i);
			}
			if (ImGui::TreeNode(name.c_str()))
			{
				_BoneView(i);
				ImGui::TreePop();
			}
		}
		ImGui::TreePop();
	}
}
void Model::_BoneView(int i) {
	for (unsigned int ii = 0; ii < this->pmx->bone_count; ii++) {
		if (i == this->pmx->bones[ii].parent_index) {
			std::string name;
			narrow(this->pmx->bones[ii].bone_english_name, name);
			if (name == "") {
				name = "UnkownBone : " + std::to_string(ii);
			}
			if (ImGui::TreeNode(name.c_str())) {
				_BoneView(ii);
				ImGui::TreePop();
			}
		}
	}
}

void  Model::setDrawShadow(bool flg){
	this->drawshadow = flg;
}
bool Model::isDrawShadow() {
	return this->drawshadow;
}
float Model::getGlobalScale() {
	return this->globalscale;
}
void Model::setGlobalScale(float sc) {
	this->globalscale = sc;
}