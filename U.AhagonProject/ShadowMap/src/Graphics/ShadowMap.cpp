#include <./Device/DirectXDevice.h>
#include <./Device/WindowDevice.h>
#include <./Graphics/ShadowMap.h>
#include <./Graphics/DXStates.h>
#include <./Math/Vertex.h>
#include <./GUI/imgui.h>
#include <./GUI/imgui_internal.h>
#include <./GUI/imgui_impl_dx11.h>
#include <xnamath.h>

// Shaderに送るカメラ情報
struct ConstantCameraBuffer{
	XMMATRIX mView;			//ビュー変換行列
	XMMATRIX mProjection;	//透視投影変換行列
};
ShadowMap::ShadowMap() {
	this->setOrtho(false);
	this->fov = 45.0f;
	this->NearZ = 1.0f;
	this->FarZ = 1000.0f;
	
	this->camera_pos[0] = 0.0f;
	this->camera_pos[1] = 20.0f;
	this->camera_pos[2] = -30.0f;
	this->camera_at[0] =  0.0f;
	this->camera_at[1] =  10.0f;
	this->camera_at[2] =  0.0f;
	this->clearcolor[0] = 0.0f;
	this->clearcolor[1] = 0.0f;
	this->clearcolor[2] = 0.0f;
	this->clearcolor[3] = 0.0f;
}
ShadowMap::~ShadowMap() {
	this->camera_at[2] =  0.0f;
}
int ShadowMap::Update() {
	DX11Device &device = DX11Device::getInstance();
	WindowDevice &window = WindowDevice::getInstance();
	DEBUG(if(this->isDebug()) {
	ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.25f, 0.25f, 0.25f, 1.0));
	ImGui::Begin("Shadow Map Debug Rendering", nullptr,ImGuiWindowFlags_NoScrollbar);
	ImGui::DragFloat4("Clear Color", this->clearcolor,0.01f, 0.0f, 10.0f, "%.2f");
	if (ImGui::Button((this->isOrtho() ? "Ortho" : "Proj"))) {
		this->setOrtho(!this->isOrtho());
	}
	ImGui::DragFloat3("Camera Pos",camera_pos,0.1f);
	ImGui::DragFloat3("Camera At",camera_at,0.1f);
	ImGui::DragFloat("Near Z",&NearZ,0.1f);
	ImGui::DragFloat("Far Z",&FarZ,0.01f);
	ImGui::DragFloat("Fov", &fov, 0.1f);
	ImGui::Image((void*) this->buffer.getSRV(), ImVec2((float) this->getWidth()*0.25f, (float) this->getHeight()*0.25f));
	ImGui::End();
	ImGui::PopStyleColor(1);
	});

	ConstantCameraBuffer cameramtx;
	cameramtx.mView = XMMatrixLookAtLH(XMVectorSet(camera_pos[0]*0.1f,camera_pos[1]*0.1f,camera_pos[2]*0.1f,1.0f),XMVectorSet(camera_at[0]*0.1f,camera_at[1]*0.1f,camera_at[2]*0.1f,1.0f) , XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f));
	camera.InitProjMatrix(D3DXToRadian(fov), (float) window.getWidth(), (float) window.getHeight(), NearZ, FarZ);
	camera.InitOrthoMatrix((float) window.getWidth(), (float) window.getHeight(), NearZ, FarZ);

	if (this->isOrtho()) {
		cameramtx.mProjection = Game::GMathFM((XMFLOAT4X4) camera.Ortho());
	}
	else {
		cameramtx.mProjection = Game::GMathFM((XMFLOAT4X4) camera.Proj());

	}
	cameramtx.mView = XMMatrixTranspose(cameramtx.mView);
	device.getContext()->UpdateSubresource(this->camerabuf.GetInterfacePtr(), 0, NULL, &cameramtx, 0, 0); // CPU によって、マッピング不可能なメモリー内に作成されたサブリソースにメモリーからデータがコピーされる

	return 0;
}
bool ShadowMap::Create(ULONG width, ULONG height){
	DX11Device &device = DX11Device::getInstance();
	this->buffer.Create(width, height, D3D11_USAGE_DEFAULT, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET);
	this->depth.Create(width, height, D3D11_USAGE_DEFAULT, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET);
	D3D11_BUFFER_DESC bd; // 生成方法(バッファー リソース)
	ZeroMemory(&bd, sizeof(bd)); // 中身をクリア
	// Bufferの生成方法の格納
	bd.ByteWidth = sizeof(ConstantCameraBuffer); // sizeの指定
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER; // なんのバッファですか？
	bd.CPUAccessFlags = 0; // CPUからは書き込みのみ行います
	bd.MiscFlags = 0;
	bd.StructureByteStride = sizeof(float);
	device.getDevice()->CreateBuffer(&bd, NULL, &this->camerabuf); // バッファの生成
	this->width = width;
	this->height = height;
	return true;
}
bool ShadowMap::CreatePipeLine(){
	DX11Device &device = DX11Device::getInstance();
	WindowDevice &window = WindowDevice::getInstance();
	// ラスライザの設定
	D3D11_RASTERIZER_DESC rasterizerDesc = {
		D3D11_FILL_SOLID, // ワイヤーフレーム (レンダリング時に使用する描画モードを決定)
		D3D11_CULL_NONE, // 裏面ポリゴンをカリング(指定の方向を向いている三角形が描画されないことを示す)
		FALSE,			  // 三角形が前向きか後ろ向きかを決定する
		0,				  // 指定のピクセルに加算する深度値
		0.0f,             // ピクセルの最大深度バイアス
		FALSE,			  // 指定のピクセルのスロープに対するスカラ
		FALSE,			  // 距離に基づいてクリッピングを有効
		FALSE,            // シザー矩形カリングを有効
		FALSE,			  // マルチサンプリングのアンチエイリアシングを有効
		FALSE			  //　線のアンチエイリアシングを有効
	};
	Rasterizer *rast = new Rasterizer;
	// ラスタライザの生成
	rast->Create(&rasterizerDesc); 
	// パイプラインにラスタライザを追加
	this->gpl.Attach(rast);

	// サンプラーの生成
	D3D11_SAMPLER_DESC sampDesc;
	//サンプラーの設定
	sampDesc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR;;	//変更
	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
	//sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	//sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	//sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sampDesc.BorderColor[0] = 1.0f; 
	sampDesc.BorderColor[1] = 1.0f;
	sampDesc.BorderColor[2] = 1.0f;
	sampDesc.BorderColor[3] = 1.0f;
	sampDesc.MipLODBias = 0.0f;
	sampDesc.MaxAnisotropy = 2;
	sampDesc.MinLOD = FLT_MAX * -1;
	sampDesc.MaxLOD = FLT_MAX;
	sampDesc.ComparisonFunc = D3D11_COMPARISON_LESS_EQUAL;			//変更

	Sampler *samp = new Sampler;
	samp->Create(0, &sampDesc);
	samp->Attach(1, samp, 0);
	samp->Attach(2, samp, 0);
	samp->Attach(3, samp, 0);
	samp->SetStatus();


	// パイプラインにサンプラーを追加
	this->gpl.Attach(samp);
	//シェーダーの生成
	ID3D10Blob  *vsblobfinal, *psblobfinal;
	ID3D11VertexShader *vs_buffer;
	ID3D11PixelShader *ps_buffer;
	D3DX11CompileFromFile(TEXT("./Shader/Shadow/VSShadowMap.hlsl"), 0, 0, "main", "vs_5_0", 0, 0, 0, &vsblobfinal, 0, 0);
	D3DX11CompileFromFile(TEXT("./Shader/Shadow/PSShadowMap.hlsl"), 0, 0, "main", "ps_5_0", 0, 0, 0, &psblobfinal, 0, 0);
	// blobを_bufに格納
	device.getDevice()->CreateVertexShader(vsblobfinal->GetBufferPointer(), vsblobfinal->GetBufferSize(), nullptr, &vs_buffer); // コンパイル済みシェーダーから、頂点シェーダー オブジェクトを作成
	device.getDevice()->CreatePixelShader(psblobfinal->GetBufferPointer(), psblobfinal->GetBufferSize(), nullptr, &ps_buffer);  // ピクセル シェーダーを作成
	// Shagerの設定

	this->vs = std::move(vs_buffer);
	this->ps = std::move(ps_buffer);
	
	//　インプットレイアウトを使うために必要なもの
	D3D11_INPUT_ELEMENT_DESC element [] = { // 入力アセンブラー ステージの単一の要素( HLSL セマンティクス,要素のセマンティクス インデックス,要素データのデータ型,入力アセンブラーを識別する整数値,各要素間のオフセット (バイト単位),単一の入力スロットの入力データ クラスを識別,インスタンス単位の同じデータを使用して描画するインスタンスの数)
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },// POS情報
		{ "NORMAL"	, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,	12, D3D11_INPUT_PER_VERTEX_DATA, 0 },// NORMAL情報
	};
	InputLayout *input = new InputLayout;
	input->Create(element, ARRAYSIZE(element), vsblobfinal);
	this->gpl.setPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST); // 頂点の並び方の設定(プリミティブ タイプおよびデータの順序に関する情報をバインド)
	this->gpl.Attach(input);

	
	ViewPort *viewport = new ViewPort();
	viewport->SetView(0, 0, this->getWidth() , this->getHeight());
	viewport->SetDepth(0, 1.0f);
	this->gpl.Attach(viewport);
	return true;
}
void ShadowMap::Clear(){
	DX11Device &device = DX11Device::getInstance();
	device.getContext()->ClearRenderTargetView(this->getBuffer().getRTV(), this->clearcolor);
	device.getContext()->ClearDepthStencilView(this->depth.getDSV(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
}
void ShadowMap::Clear(float color[4]){
	DX11Device &device = DX11Device::getInstance();
	device.getContext()->ClearRenderTargetView(this->getBuffer().getRTV(), color);
	device.getContext()->ClearDepthStencilView(this->depth.getDSV(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
}
void ShadowMap::Clear(float r,float g,float b,float a){
	DX11Device &device = DX11Device::getInstance();
	float color[4] = {
		r,g,b,a
	};
	device.getContext()->ClearRenderTargetView(this->getBuffer().getRTV(), color);
	device.getContext()->ClearDepthStencilView(this->depth.getDSV(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
}
void ShadowMap::setStatus(){
	DX11Device &device = DX11Device::getInstance();
	this->gpl.ClearPipeLine();
	this->gpl.setStatus();
	ID3D11Buffer *buf [] = {
		this->camerabuf.GetInterfacePtr()
	};
	device.getContext()->VSSetConstantBuffers(0, 1, buf);
	
	ID3D11RenderTargetView *rtv [] = {
		this->buffer.getRTV()
	};
	device.getContext()->OMSetRenderTargets(1,  rtv,this->depth.getDSV());
}
void ShadowMap::setShader() {
	DX11Device &device = DX11Device::getInstance();
	device.getContext()->VSSetShader(this->vs.GetInterfacePtr(),nullptr,0);
	device.getContext()->PSSetShader(this->ps.GetInterfacePtr(),nullptr,0);
}
Texture2D &ShadowMap::getBuffer() {
	return this->buffer;
}
void ShadowMap::Draw() {
	DX11Device &device = DX11Device::getInstance();

}
void ShadowMap::setOrtho(bool flg) {
	this->isortho = flg;
	this->isproj = !flg;
}
void ShadowMap::setProj(bool flg) {
	this->isproj = flg;
	this->isortho = !flg;
}
bool ShadowMap::isOrtho() {
	return this->isortho;
}
bool ShadowMap::isProj() {
	return this->isproj;
}
float ShadowMap::getWidth() {
	return this->width;
}
float ShadowMap::getHeight() {
	return this->height;
}