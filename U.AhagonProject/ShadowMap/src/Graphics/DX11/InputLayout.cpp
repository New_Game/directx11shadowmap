#include <./Graphics/DX11/InputLayout.h>
#include <./Device/DirectXDevice.h>

InputLayout::InputLayout()
{
}

InputLayout::~InputLayout()
{
}
bool InputLayout::Create(D3D11_INPUT_ELEMENT_DESC *element,UINT num, ID3D10Blob *blob){
	if (element == nullptr || this->inputlayout){
		return false;
	}
	DX11Device &device = DX11Device::getInstance();
	if (FAILED(device.getDevice()->CreateInputLayout(element, num, blob->GetBufferPointer(), blob->GetBufferSize(), &this->inputlayout))){
		return false;
	}
	return true;
}
bool InputLayout::SetStatus(){
	if (!this->inputlayout){
		return false;
	}
	DX11Device &device = DX11Device::getInstance();
	device.getContext()->IASetInputLayout(this->inputlayout);
	return true;
}
bool InputLayout::SetStatus(ID3D11DeviceContext* context){
	if (!this->inputlayout){
		return false;
	}
	context->IASetInputLayout(this->inputlayout);
	return true;
}

void InputLayout::AddRef(){
	if (!this->inputlayout){
		return;
	}
	this->inputlayout->AddRef();
}

void InputLayout::Release(){
	if (!this->inputlayout){
		return;
	}
	this->inputlayout->Release();
	this->inputlayout = nullptr;
}