#include <./Graphics/DX11/ViewPort.h>
#include <./Device/DirectXDevice.h>

ViewPort::ViewPort(){
}

ViewPort::~ViewPort(){
}


bool ViewPort::SetStatus(){
	DX11Device &device = DX11Device::getInstance();
	device.getContext()->RSSetViewports(1, &this->viewport); // パイプラインのラスタライザー ステージにビューポートの配列をバインド
	return true;
}
bool ViewPort::SetStatus(ID3D11DeviceContext* context){
	if (context == nullptr)
		return false;
	context->RSSetViewports(1, &this->viewport); // パイプラインのラスタライザー ステージにビューポートの配列をバインド
	return true;
}

void ViewPort::AddRef(){
}

void ViewPort::Release(){
}

void ViewPort::SetView(float topx, float topy, float Width, float height) {
	this->viewport.TopLeftX = topx;
	this->viewport.TopLeftY = topy;
	this->viewport.Width = Width;
	this->viewport.Height = height;
}
void ViewPort::SetDepth(float min, float max) {
	this->viewport.MinDepth = min;
	this->viewport.MaxDepth = max;
}