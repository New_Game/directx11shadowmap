struct VSOUT{
	float4 pos : SV_POSITION;
	float3 normal  : NORMAL;
};
float4 main(VSOUT inp) : SV_TARGET{
	float z = inp.pos.z / inp.pos.w;
	return float4(z,z,z,1.0f);
}