cbuffer _Camera : register(b0){
	matrix View;
	matrix Projection;
};
cbuffer _World : register(b1){
	matrix World;
};
cbuffer _Shadow :register(b2) {
	matrix ShadowView;
	matrix ShadowProjection;
};

// VertexShaderInput(���j
struct VSIN{
	float3 pos     : POSITION;
	float3 normal  : NORMAL;
};
// VertexShaderOutput(�o)
struct VSOUT{
	float4 pos     : SV_POSITION;
	float3 normal  : NORMAL;
};
VSOUT main(VSIN inp){
	VSOUT outp = (VSOUT)0;
	outp.pos.xyz = inp.pos;
	outp.normal.xyz = inp.normal;
	outp.pos.w = 1.0f;
	outp.pos = mul(outp.pos, (float4x4)World);
	outp.pos = mul(outp.pos, (float4x4)ShadowView);
	outp.pos = mul(outp.pos, (float4x4)ShadowProjection);
	//outp.pos = normalize(outp.pos);

	outp.normal = mul(outp.normal,(float4x4)World);
	outp.normal = normalize(outp.normal);


	return outp;
}