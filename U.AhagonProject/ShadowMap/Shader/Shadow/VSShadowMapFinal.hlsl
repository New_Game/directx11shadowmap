
// VertexShaderInput(���j
struct VSIN{
	float4 pos : SV_POSITION;
	float2 uv  : TEXCOORD0;
};
// VertexShaderOutput(�o)
struct VSOUT{
	float4 pos : SV_POSITION;
	float2 uv  : TEXCOORD;
};
VSOUT main(VSIN inp){
	VSOUT outp = (VSOUT)0;
	outp.pos = inp.pos;
	outp.uv = inp.uv;

	return outp;
}