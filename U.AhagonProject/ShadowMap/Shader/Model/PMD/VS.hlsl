cbuffer _Camera : register(b0){
	matrix View;
	matrix Projection;
};
cbuffer _World : register(b1){
	matrix World;
};
// VertexShaderInput(���j
struct VSIN{
	float3 pos     : POSITION;
	float3 normal  : NORMAL;
	float2 uv      : TEXCOORD0;
};
// VertexShaderOutput(�o)
struct VSOUT{
	float4 pos     : SV_POSITION;
	float3 normal  : NORMAL;
	float2 uv      : TEXCOORD0;
};
VSOUT main(VSIN inp){
	VSOUT outp = (VSOUT)0;
	outp.pos.xyz = inp.pos;
	outp.pos.w = 1.0f;
	outp.pos = mul(outp.pos, (float4x4)World);
	outp.pos = mul(outp.pos, (float4x4)View);
	outp.pos = mul(outp.pos, (float4x4)Projection);


	outp.normal = mul(inp.normal,(float4x4)World);
	outp.normal = normalize(outp.normal);

	outp.uv   = inp.uv;
	return outp;
}