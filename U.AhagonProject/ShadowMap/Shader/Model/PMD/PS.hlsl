cbuffer MaterialBuffer : register(b0){

	/// 減衰色
	float4 diffuse;
	/// 光沢度
	float power;
	/// 光沢色
	float3 specular;
	/// 環境色
	float3 ambient;
	/// トーンインデックス
	//short toon_index;
	///// エッジ
	//short edge_flag;
};
struct VSOUT{
	float4 pos : SV_POSITION;
	float3 normal  : NORMAL;
	float2 uv      : TEXCOORD0;
};
struct PSOUT {
	float4 albed :SV_TARGET0;
	float4 normal :SV_TARGET1;
	float4 depth :SV_TARGET2;
	float4 diffuse :SV_TARGET3;
};
SamplerState samLinear : register(s0); // どういうふうに大きくするか　(S)サンプラーが入ってるレジスター
Texture2D texDiffuse   : register(t0);  // (T)テクスチャが入ってるレジスター
Texture2D texSphere    : register(t1); 
Texture2D texToon      : register(t2); 
//Texture2D texNormal  :register(t1);

PSOUT main(VSOUT inp){
	float4 light_dir = float4(0.0f, -0.5f, -0.5f, 1.0f);
	float4 light_color = float4(0.5f, 0.5f, 0.5f, 1.0f);
	PSOUT outp = (PSOUT) 0;
	outp.albed = texDiffuse.Sample(samLinear, inp.uv);
	outp.albed.rgb = ambient * outp.albed.rgb +specular;
	
	outp.normal.rgb = inp.normal;
	outp.normal.a = 1.0f;
	outp.depth = (inp.pos.z/inp.pos.w);
	outp.normal.a = (inp.pos.z/inp.pos.w);
	outp.diffuse = diffuse;
	
	return outp;
}