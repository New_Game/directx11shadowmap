cbuffer _Camera : register(b0){
	matrix View;
	matrix Projection;
};
cbuffer _World : register(b1){
	matrix World;
};
cbuffer _Shadow :register(b2) {
	matrix ShadowView;
	matrix ShadowProjection;
};

// VertexShaderInput(入）
struct VSIN{
	float3 pos     : POSITION;
	float3 normal  : NORMAL;
	float2 uv      : TEXCOORD0;
	float4 uva0    : UVA0;
	float4 uva1    : UVA1;
	float4 uva2    : UVA2;
	float4 uva3    : UVA3;
};
// VertexShaderOutput(出)
struct VSOUT{
	float4 pos     : SV_POSITION;
	float4 SdwCoord: POSITION0;
	float3 normal  : NORMAL;
	float2 uv      : TEXCOORD0;
	float4 uva0    : UVA0;
	float4 uva1    : UVA1;
	float4 uva2    : UVA2;
	float4 uva3    : UVA3;
};

VSOUT main(VSIN inp){

	VSOUT outp = (VSOUT)0;
	outp.pos.xyz = inp.pos;
	outp.normal.xyz = inp.normal;
	outp.pos.w = 1.0f;
	outp.pos = mul(outp.pos, (float4x4)World);
	float4 shadow = outp.pos;
	outp.pos = mul(outp.pos, (float4x4)View);
	outp.pos = mul(outp.pos, (float4x4)Projection);
	//outp.pos = normalize(outp.pos);

	//ライトから見たビュー変換
	outp.SdwCoord = mul(shadow, (float4x4)ShadowView);
	outp.SdwCoord = mul(outp.SdwCoord, (float4x4)ShadowProjection);
	//outp.SdwCoord = normalize(outp.SdwCoord);


	outp.normal = mul(outp.normal,(float4x4)World);
	outp.normal = mul(outp.normal, (float4x4)ShadowView);
	outp.normal = normalize(outp.normal);
	

	outp.uv   = inp.uv;
	outp.uva0 = inp.uva0;
	outp.uva1 = inp.uva1;
	outp.uva2 = inp.uva2;
	outp.uva3 = inp.uva3;
	return outp;
}